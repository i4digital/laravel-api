<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ErrorController extends Controller
{
    
    public function error401(){
        return response()->json([
            'success'   => 'false',
            'code'      => 401,
            'messages'  => 'Unauthorized'
        ]);
    }
}
