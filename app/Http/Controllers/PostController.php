<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        return Post::all();
    }

    /**
     * Post publish, paginate and the lastes 5 comments
     */
    public function paginate(){
        return Post::with( ['comments' => function($c){
            $c->latest()->limit(5)->get() ;
        } ])->where('is_publish', 'true')->paginate(10);
    }


    public function paginateProtected(){
        return Post::where('is_publish', 'true')->paginate(10);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $post = Post::create($request->all());
            return response()->json([
                'success' => 'true',
                'messages' => 'Post created'
            ]);
        }catch(\Exception $e){
            return response()->json([
                'success' => false,
                'messages' => "Error to save ".$e->getMessage()
            ]);
        }
    }

    public function getPostActive($id){
        return Post::with(['comments'])->where('id',$id)->where('is_publish',true)->get();
    }

    public function getPost($id){
        return Post::with(['comments'])->where('id',$id)->get();
    }

    public function update(Request $request, $id){
        $post = Post::find($id);
        if($post->user()->id == Auth::user()->id){
            $post->fill($request->all())->save();
            return response()->json([
                'success' => 'true',
                'messages' => 'save success'
            ]);
        }else{
            return response()->json([
                'success' => 'false',
                'messages' => 'You do not have permission'
            ]);
        }
        
        
    }

    public function destroy($id)
    {
        $post = Post::find($id);

        if($post->user()->id == Auth::user()->id){
            $post->delete();
            return response()->json([
                'success' => 'true',
                'messages' => 'post delete'
            ]);
        }else{
            return response()->json([
                'success' => 'false',
                'messages' => 'You do not have permission'
            ]);
        }
    }
    
}
