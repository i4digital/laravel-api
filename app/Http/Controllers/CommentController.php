<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Comment;
class CommentController extends Controller
{
    public function getCommentsByPostId($postId){
        return Comment::where('post_id', $postId)
        ->where('is_publish', true)
        ->paginate(10);
    }

    public function allCommentByPostId($postId){
        return Comment::where('post_id', $postId)
        ->paginate(10);
    }

    public function store(Request $request)
    {
        try{
            Comment::create($request->all());
            return response()->json([
                'success' => 'true',
                'messages' => 'Comment created'
            ]);
        }catch(\Exception $e){
            return response()->json([
                'success' => false,
                'messages' => "Error to save ".$e->getMessage()
            ]);
        }
    }

    public function update(Request $request, $id){
        $comment = Comment::find($id);
        if($comment->user()->id == Auth::user()->id){
            $comment->fill($request->all())->save();
            return response()->json([
                'success' => 'true',
                'messages' => 'save success'
            ]);
        }else{
            return response()->json([
                'success' => 'false',
                'messages' => 'You do not have permission'
            ]);
        }
        
        
    }

    public function destroy($id)
    {
        $comment = Comment::find($id);

        if($comment->user()->id == Auth::user()->id){
            $comment->delete();
            return response()->json([
                'success' => 'true',
                'messages' => 'comment delete'
            ]);
        }else{
            return response()->json([
                'success' => 'false',
                'messages' => 'You do not have permission'
            ]);
        }
    }
}
