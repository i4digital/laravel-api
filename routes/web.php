<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/error.401','ErrorController@error401')->name('error.401');

/*


*/

Route::group(['prefix' =>'api'],function(){
    Route::get('/posts/all','PostController@index')->name('posts.all');
    Route::get('/posts/published','PostController@paginate')->name('posts.published');
    Route::get('/post/{id}', 'PostController@getPostActive')->name('getPost');
    Route::post('/post', 'PostController@store')->name('post.store');

    Route::get('/comments/{postid}', 'CommentController@getCommentsByPostId')->name('comments.post');

});

Route::prefix('api/protected')->group(function () {
    Route::middleware(['check.auth'])->group(function () {
        Route::get('/post/published','PostController@paginateProtected')->name('posts.protected.published');
        Route::get('/post/{id}', 'PostController@getPost')->name('getPost');
        Route::post('/post', 'PostController@store')->name('post.store');
        Route::delete('/post/{id}', 'PostController@destroy')->name('post.delete');

        Route::get('/comments/{commentId}', 'CommentController@allCommentByPostId')->name('comments.post.all');
        Route::post('/comment', 'CommentController@store')->name('comment.store');
        Route::delete('/comment/{id}', 'CommentController@destroy')->name('comment.delete');
    });
});




